package org.nox.admin.service;

import org.nox.admin.entity.SysRolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色权限关联表 服务类
 * </p>
 *
 * @author nox
 * @since 2018-12-09
 */
public interface ISysRolePermissionService extends IService<SysRolePermission> {

}
