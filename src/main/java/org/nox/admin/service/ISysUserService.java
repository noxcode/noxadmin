package org.nox.admin.service;

import org.nox.admin.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统用户表 服务类
 * </p>
 *
 * @author nox
 * @since 2018-12-09
 */
public interface ISysUserService extends IService<SysUser> {

}
