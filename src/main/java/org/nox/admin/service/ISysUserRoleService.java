package org.nox.admin.service;

import org.nox.admin.entity.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户角色关联表 服务类
 * </p>
 *
 * @author nox
 * @since 2018-12-09
 */
public interface ISysUserRoleService extends IService<SysUserRole> {

}
