package org.nox.admin.service.impl;

import org.nox.admin.entity.SysUserRole;
import org.nox.admin.mapper.SysUserRoleMapper;
import org.nox.admin.service.ISysUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色关联表 服务实现类
 * </p>
 *
 * @author nox
 * @since 2018-12-09
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {

}
