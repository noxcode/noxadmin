package org.nox.admin.service.impl;

import org.nox.admin.entity.SysLog;
import org.nox.admin.mapper.SysLogMapper;
import org.nox.admin.service.ISysLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 操作日志 服务实现类
 * </p>
 *
 * @author nox
 * @since 2018-12-09
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements ISysLogService {

}
