package org.nox.admin.service.impl;

import org.nox.admin.entity.SysPermission;
import org.nox.admin.mapper.SysPermissionMapper;
import org.nox.admin.service.ISysPermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统权限表 服务实现类
 * </p>
 *
 * @author nox
 * @since 2018-12-09
 */
@Service
public class SysPermissionServiceImpl extends ServiceImpl<SysPermissionMapper, SysPermission> implements ISysPermissionService {

}
