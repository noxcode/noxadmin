package org.nox.admin.service.impl;

import org.nox.admin.entity.SysRole;
import org.nox.admin.mapper.SysRoleMapper;
import org.nox.admin.service.ISysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统角色表 服务实现类
 * </p>
 *
 * @author nox
 * @since 2018-12-09
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

}
