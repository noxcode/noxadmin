package org.nox.admin.service.impl;

import org.nox.admin.entity.SysUser;
import org.nox.admin.mapper.SysUserMapper;
import org.nox.admin.service.ISysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统用户表 服务实现类
 * </p>
 *
 * @author nox
 * @since 2018-12-09
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {

}
