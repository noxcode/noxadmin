package org.nox.admin.common.config;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.unit.DataSize;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.MultipartConfigElement;
import java.nio.charset.Charset;
import java.util.List;

@Configuration
public class WebConifg  implements WebMvcConfigurer {

    /**
     * 配置Cors跨域
     * @param registry
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowCredentials(true)
                .allowedMethods("GET", "POST", "DELETE", "PUT")
                .maxAge(3600);
    }

    /**
     * 图片上传配置
     * @return
     */
    @Bean
    public MultipartConfigElement multipartConfigElement(){
        MultipartConfigFactory factory = new MultipartConfigFactory();
        //文件最大KB
        factory.setMaxFileSize(DataSize.ofKilobytes(2048));
        //设置总上传数据总大小KB
        factory.setMaxRequestSize(DataSize.ofKilobytes(10240));

        return factory.createMultipartConfig();
    }

    /**
     * 去除返回字符串有双引号
     * @return
     */
    public HttpMessageConverter<String> stringConverter() {
        StringHttpMessageConverter converter = new StringHttpMessageConverter(
                Charset.forName("UTF-8"));
        return converter;
    }



    public HttpMessageConverter fastHttpMessageConverters(){
        //创建转换对象
        FastJsonHttpMessageConverter converter = new FastJsonHttpMessageConverter();
        //创建配置文件对象
        FastJsonConfig config = new FastJsonConfig();
        // 结果格式化
        config.setSerializerFeatures(SerializerFeature.PrettyFormat);
        // 对象字段为null也显示
        config.setSerializerFeatures(SerializerFeature.WriteMapNullValue);
        config.setCharset(Charset.forName("UTF-8"));
        converter.setFastJsonConfig(config);
        return converter;
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        // 将字符串对象去除双引号
        converters.add(stringConverter());
        // 使用fasgjson
        converters.add(fastHttpMessageConverters());
    }



}
