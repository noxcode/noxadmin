package org.nox.admin.common.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PerformanceInterceptor;
import com.baomidou.mybatisplus.extension.plugins.SqlExplainInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.Properties;

/**
 * mybatis配置
 *
 * @author Nox
 */
@Configuration
@EnableTransactionManagement
@MapperScan("org.nox.admin.mapper")
public class MybatisPlusConfig {

    /**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

    /**
     * SQL执行效率插件
     */
    @Bean
    @Profile({"dev","test"})// 设置 dev test 环境开启
    public PerformanceInterceptor performanceInterceptor() {
        return new PerformanceInterceptor();
    }

    /**
     * SQL 执行分析拦截器 stopProceed 发现全表执行 delete update 是否停止运行
     * @return
     */
    @Bean
    @Profile({"dev","test"})// 设置 dev test 环境开启
    public SqlExplainInterceptor sqlExplainInterceptor() {
        SqlExplainInterceptor interceptor = new SqlExplainInterceptor();
        Properties properties = new Properties();
        properties.setProperty("stopProceed", "false");
        interceptor.setProperties(properties);
        return interceptor;
    }
}
