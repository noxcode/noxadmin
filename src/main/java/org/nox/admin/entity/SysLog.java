package org.nox.admin.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 操作日志
 * </p>
 *
 * @author nox
 * @since 2018-12-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
//@Accessors(chain = true)
@ApiModel(value="SysLog对象", description="操作日志")
public class SysLog extends Model<SysLog> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "编号")
    @TableId(value = "log_id", type = IdType.AUTO)
    private Integer logId;

    @ApiModelProperty(value = "操作描述")
    private String description;

    @ApiModelProperty(value = "操作用户")
    private String username;

    @ApiModelProperty(value = "操作时间")
    private LocalDateTime startTime;

    @ApiModelProperty(value = "消耗时间")
    private LocalDateTime spendTime;

    @ApiModelProperty(value = "根路径")
    private String basePath;

    @ApiModelProperty(value = "uri")
    private String uri;

    @ApiModelProperty(value = "url")
    private String url;

    @ApiModelProperty(value = "请求类型")
    private String method;

    @ApiModelProperty(value = "请求参数")
    private String parameter;

    @ApiModelProperty(value = "ip地址")
    private String ip;

    @ApiModelProperty(value = "请求结果")
    private String result;

    @ApiModelProperty(value = "权限值")
    private String permissions;


    @Override
    protected Serializable pkVal() {
        return this.logId;
    }

}
