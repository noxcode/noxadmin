package org.nox.admin.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 角色权限关联表
 * </p>
 *
 * @author nox
 * @since 2018-12-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="SysRolePermission对象", description="角色权限关联表")
public class SysRolePermission extends Model<SysRolePermission> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "编号")
    @TableId(value = "role_permission_id", type = IdType.AUTO)
    private Integer rolePermissionId;

    @ApiModelProperty(value = "角色编号")
    private Integer roleId;

    @ApiModelProperty(value = "权限编号")
    private Integer permissionId;


    @Override
    protected Serializable pkVal() {
        return this.rolePermissionId;
    }

}
