package org.nox.admin.mapper;

import org.nox.admin.entity.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统角色表 Mapper 接口
 * </p>
 *
 * @author nox
 * @since 2018-12-09
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
