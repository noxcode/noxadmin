package org.nox.admin.mapper;

import org.nox.admin.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统用户表 Mapper 接口
 * </p>
 *
 * @author nox
 * @since 2018-12-09
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
