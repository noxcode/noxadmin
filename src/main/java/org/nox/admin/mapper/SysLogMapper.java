package org.nox.admin.mapper;

import org.nox.admin.entity.SysLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 操作日志 Mapper 接口
 * </p>
 *
 * @author nox
 * @since 2018-12-09
 */
public interface SysLogMapper extends BaseMapper<SysLog> {

}
