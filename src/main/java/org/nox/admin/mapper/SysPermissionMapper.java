package org.nox.admin.mapper;

import org.nox.admin.entity.SysPermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统权限表 Mapper 接口
 * </p>
 *
 * @author nox
 * @since 2018-12-09
 */
public interface SysPermissionMapper extends BaseMapper<SysPermission> {

}
