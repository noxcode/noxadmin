package org.nox.admin.mapper;

import org.nox.admin.entity.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户角色关联表 Mapper 接口
 * </p>
 *
 * @author nox
 * @since 2018-12-09
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
