package org.nox.admin.mapper;

import org.nox.admin.entity.SysRolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色权限关联表 Mapper 接口
 * </p>
 *
 * @author nox
 * @since 2018-12-09
 */
public interface SysRolePermissionMapper extends BaseMapper<SysRolePermission> {

}
