package org.nox.admin.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.extension.api.ApiController;

/**
 * <p>
 * 系统角色表 前端控制器
 * </p>
 *
 * @author nox
 * @since 2018-12-09
 */
@RestController
@RequestMapping("/sys-role")
public class SysRoleController extends ApiController {

}

