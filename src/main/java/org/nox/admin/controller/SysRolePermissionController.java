package org.nox.admin.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.extension.api.ApiController;

/**
 * <p>
 * 角色权限关联表 前端控制器
 * </p>
 *
 * @author nox
 * @since 2018-12-09
 */
@RestController
@RequestMapping("/sys-role-permission")
public class SysRolePermissionController extends ApiController {

}

