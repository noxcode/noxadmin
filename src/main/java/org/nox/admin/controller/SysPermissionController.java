package org.nox.admin.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.extension.api.ApiController;

/**
 * <p>
 * 系统权限表 前端控制器
 * </p>
 *
 * @author nox
 * @since 2018-12-09
 */
@RestController
@RequestMapping("/sys-permission")
public class SysPermissionController extends ApiController {

}

